﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hand : MonoBehaviour
{
    public GameObject main;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "rob")
        {
            main.GetComponent<Main>().touchrob = true;
            this.GetComponent<Animator>().Play("hold_2");
            StartCoroutine(wait());
            Destroy(other.gameObject);
        }
        if (other.tag == "bucket")
        {
            main.GetComponent<Main>().bucketready = true;
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "nipple")
        {
            main.GetComponent<Main>().touchnipple = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "nipple")
        {
            main.GetComponent<Main>().touchnipple = false;
            
        }
    }
    IEnumerator wait()
    {
        yield return new WaitForSeconds(0.5f);
        this.GetComponent<Animator>().Play("idle");
    }
}
