﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sound : MonoBehaviour
{
    public float times;
    public float change;

    // Start is called before the first frame update
    void Start()
    {
        change = Random.Range(10, 60);
    }

    // Update is called once per frame
    void Update()
    {
        times = times + 1f * Time.deltaTime;

        if (times >= change)
        {
            this.GetComponent<AudioSource>().Play();
            change = Random.Range(10, 60);
            times = 0;
        }
    }
}
