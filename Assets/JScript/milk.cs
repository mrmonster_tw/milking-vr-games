﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class milk : MonoBehaviour
{
    public GameObject milk_1;
    public GameObject milk_2;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void milk_fn(int xx)
    {
        if (xx == 0)
        {
            GameObject milk_1_cn = Instantiate(milk_1);
            milk_1_cn.transform.position = new Vector3(-0.7849f, 0.595f, 1.093f);
            milk_1_cn.transform.eulerAngles = new Vector3(87, 90, -90);
        }
        if (xx == 1)
        {
            GameObject milk_2_cn = Instantiate(milk_2);
            milk_2_cn.transform.position = new Vector3(-0.9174f, 0.5939f, 1.0975f);
            milk_2_cn.transform.eulerAngles = new Vector3(85.577f, 90, -90);
        }
    }
}
