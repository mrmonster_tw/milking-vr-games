﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cow : MonoBehaviour
{
    public bool eat;
    public bool idle_1;
    public bool idle_2;
    public bool walk;
    public float speed = 0.2f;

    public float times;
    public float change;

    // Start is called before the first frame update
    void Start()
    {
        change = Random.Range(10,20);
    }

    // Update is called once per frame
    void Update()
    {
        if (idle_1)
        {
            times = times + 1f * Time.deltaTime;
            if (eat)
            {
                idle_1 = false;
                GetComponent<Animator>().SetBool("eat", true);
            }
            if (eat == false)
            {
                idle_1 = true;
                GetComponent<Animator>().SetBool("eat", false);
            }

            if (eat == false && times >= change)
            {
                times = 0;
                eat = true;
                change = Random.Range(5, 10);
            }
            if (eat && times >= change)
            {
                times = 0;
                eat = false;
                change = Random.Range(10, 20);
            }
        }
        if (walk)
        {
            idle_1 = false;
            eat = false;
            GetComponent<Animator>().SetBool("walk", true);
            if (GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Arm_cow|walk"))
            {
                transform.Translate(Vector3.forward * speed * Time.deltaTime, Space.Self);
            }
            if (this.transform.position.x >= -0.4f)
            {
                walk = false;
            }
        }
        if (walk == false)
        {
            GetComponent<Animator>().SetBool("walk", false);
            idle_1 = true;
        }
    }

    void all_close()
    {
        eat = false;
        idle_1 = false;
        idle_2 = false;
        walk = false;
    }
}
