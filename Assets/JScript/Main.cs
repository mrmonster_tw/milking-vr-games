﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class Main : MonoBehaviour
{
    public bool startbl;
    public float times;
    public GameObject cowob;
    public GameObject cowsc;
    public Material mat_org;
    public Material mat_ems;
    public Material mat_ems_1;

    public GameObject word;
    public GameObject Rhand;
    public GameObject Rhand_mesh;
    public bool Rhand_trigger;
    public GameObject Lhand;
    public GameObject Lhand_mesh;
    public bool Lhand_trigger;
    public GameObject nipple_hand;
    public Animator nipple_Rhand_ani;
    public Animator nipple_Lhand_ani;
    //public Material hand_org;
    public Material hand_ems;
    public Material roborg;
    public Material robred;

    public GameObject plan;
    public float plan_time;
    public GameObject bucket;
    public GameObject bucket_2;
    int bucket_2_sound = 0;
    public GameObject bucketpati;
    public GameObject robrob;
    int robrob_sound = 0;
    public Animator cowani;

    public bool step_1;
    public bool step_2;
    public bool step_3;
    public bool step_4;
    public bool step_5;
    public bool step_6;
    public bool step_7;

    public bool touchrob;
    public bool towelready;
    public bool bucketready;
    public bool touchnipple;
    public GameObject handpoint;
    public GameObject Pillar;
    public int once = 0;

    public GameObject rob;
    public AudioClip music_2;
    public AudioClip music_3;
    public AudioClip music_4;
    public AudioClip music_5;
    public AudioClip music_6;
    public AudioClip music_7;
    public GameObject FX;
    public bool musicplay;

    public GameObject water;
    public GameObject lubricating;

    // Start is called before the first frame update
    void Start()
    {
        

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            Application.LoadLevel("Milking");
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            startbl = true;
        }
        if (startbl)
        {
            times = times + 1f * Time.deltaTime;
            if (once == 0)
            {
                word.SetActive(true);
                cowob.GetComponent<cow>().eat = false;
                cowob.GetComponent<cow>().walk = true;
                once = 1;
                this.GetComponent<AudioSource>().Play();
            }
            
        }
        if (times >= 15)
        {
            step_1 = true;
        }
        if (times >= 30)
        {
            step_1 = false;
            step_2 = true;
        }
        if (times >= 45)
        {
           
            step_2 = false;
            step_3 = true;
        }
        if (times >= 55)
        {
            
            step_3 = false;
            step_4 = true;
        }
        if (times >= 65)
        {
            
            step_4 = false;
            step_5 = true;
        }
        if (times >= 105)
        {
            step_5 = false;
            step_6 = true;
        }
        if (times >= 120)
        {
            step_6 = false;
            step_7 = true;
        }

        if (step_1)
        {
            Rhand.GetComponent<BoxCollider>().enabled = true;
            Lhand.GetComponent<BoxCollider>().enabled = true;

            if (touchrob == false)
            {
                robred.SetFloat("_des", Mathf.PingPong(Time.time * 1f, 1));
            }
            
            if (musicplay == false)
            {
                this.GetComponent<AudioSource>().PlayOneShot(music_2);
                musicplay = true;
            }
            if (robrob.activeSelf && robrob_sound == 0)
            {
                FX.GetComponent<AudioSource>().Play();
                robrob_sound = 1;
            }
        }
        if (step_2)
        {
            if (musicplay == true)
            {
                this.GetComponent<AudioSource>().PlayOneShot(music_3);

                handpoint.GetComponent<Rigidbody>().isKinematic = true;
                robred.SetFloat("_des", 0);
                handpoint.transform.position = Pillar.transform.position;
                robrob.SetActive(true);
                plan.SetActive(true);
                Rhand.GetComponent<Animator>().Play("wash");
                cowsc.GetComponent<SkinnedMeshRenderer>().material = mat_ems;
                musicplay = false;
            }

            if (robrob.activeSelf && robrob_sound == 0)
            {
                FX.GetComponent<AudioSource>().Play();
                robrob_sound = 1;
            }
            if (touchnipple && towelready == false)
            {
                plan_time = plan_time + 1 * Time.deltaTime;
                if (plan_time >= 3)
                {
                    Rhand.GetComponent<Animator>().Play("idle");
                    plan.SetActive(false);
                    FX.GetComponent<AudioSource>().Play();
                    cowsc.GetComponent<SkinnedMeshRenderer>().material = mat_org;
                    plan_time = 0;
                    towelready = true;
                }
            }
        }
        if (step_3)
        {
            if (musicplay == false)
            {
                if (towelready == false)
                {
                    FX.GetComponent<AudioSource>().Play();
                    towelready = true;
                }
                this.GetComponent<AudioSource>().PlayOneShot(music_4);

                StartCoroutine(wait_hold());
                cowsc.GetComponent<SkinnedMeshRenderer>().material = mat_org;
                bucket.SetActive(true);
                bucketpati.SetActive(true);
                musicplay = true;
            }
            if (bucketready == false)
            {
                Rhand.GetComponent<Animator>().Play("hold_2");
            }
            if (bucket_2.activeSelf && bucket_2_sound == 0)
            {
                FX.GetComponent<AudioSource>().Play();
                bucket_2_sound = 1;
            }
        }
        if (step_4)
        {
            hand_ems.SetFloat("_des", Mathf.PingPong(Time.time * 1f, 1));
            

            if (musicplay == true)
            {
                lubricating.SetActive(true);
                this.GetComponent<AudioSource>().PlayOneShot(music_5);

                Rhand.GetComponent<Animator>().Play("idle");
                bucket.SetActive(false);
                bucket_2.SetActive(true);
                bucketpati.SetActive(false);

                
                musicplay = false;
            }
            if (bucket_2.activeSelf && bucket_2_sound == 0)
            {
                FX.GetComponent<AudioSource>().Play();
                bucket_2_sound = 1;
            }
        }
        if (step_5)
        {
            if (musicplay == false)
            {
                lubricating.SetActive(false);
                FX.GetComponent<AudioSource>().Play();
                this.GetComponent<AudioSource>().PlayOneShot(music_6);
                StartCoroutine(wait_hold());
                nipple_hand.SetActive(true);
                hand_ems.SetFloat("_des", 0);
                musicplay = true;
            }
            if (Rhand_trigger)
            {
                nipple_Rhand_ani.SetBool("push",true);
            }
            if (Rhand_trigger == false)
            {
                nipple_Rhand_ani.SetBool("push", false);
            }

            if (Lhand_trigger)
            {
                nipple_Lhand_ani.SetBool("push", true);
            }
            if (Lhand_trigger == false)
            {
                nipple_Lhand_ani.SetBool("push", false);
            }

            if (water.transform.localScale.x < 300)
            {
                water.transform.localScale = new Vector3(water.transform.localScale.x + 1f * Time.deltaTime, 0.01f, water.transform.localScale.z + 1f * Time.deltaTime);
            }
            if (water.transform.localPosition.y < -45)
            {
                water.transform.localPosition = new Vector3(0, water.transform.localPosition.y + 5f * Time.deltaTime, 0);
            }
        }
        if (step_6)
        {
            if (musicplay == true)
            {
                FX.GetComponent<AudioSource>().Play();
                this.GetComponent<AudioSource>().PlayOneShot(music_7);

                cowsc.GetComponent<SkinnedMeshRenderer>().material = mat_org;
                nipple_hand.SetActive(false);
                Rhand_mesh.GetComponent<SkinnedMeshRenderer>().enabled = true;
                Lhand_mesh.GetComponent<SkinnedMeshRenderer>().enabled = true;
                musicplay = false;
            }
        }
        if (step_7)
        {
            if (once == 1)
            {
                startbl = false;
                step_7 = false;
                
                times = 0;
                cowob.GetComponent<cow>().walk = false;
                //cowob.transform.position = new Vector3(-2.37f, 0, 1.14f);
                once = 0;
            }
        }


        if (touchrob)
        {
            handpoint.transform.position = Pillar.transform.position;
            handpoint.GetComponent<Rigidbody>().isKinematic = true;
            robred.SetFloat("_des", 0);
            robrob.SetActive(true);
        }
        if (towelready)
        {
            
        }
        if (bucketready)
        {
            Rhand.GetComponent<Animator>().Play("idle");
            bucket.SetActive(false);
            bucket_2.SetActive(true);
            bucketpati.SetActive(false);
        }
        if (times >= 65 && times < 105 && touchnipple)
        {
            nipple_hand.SetActive(true);
            Rhand_mesh.GetComponent<SkinnedMeshRenderer>().enabled = false;
            Lhand_mesh.GetComponent<SkinnedMeshRenderer>().enabled = false;
            cowsc.GetComponent<SkinnedMeshRenderer>().material = mat_org;
        }
        if (times >= 65 && times < 105 && touchnipple == false)
        {
            nipple_hand.SetActive(false);
            Rhand_mesh.GetComponent<SkinnedMeshRenderer>().enabled = true;
            Lhand_mesh.GetComponent<SkinnedMeshRenderer>().enabled = true;
            cowsc.GetComponent<SkinnedMeshRenderer>().material = mat_ems_1;
        }
    }
    public void trigger_fn(int xx)
    {
        if (xx == 1)
        {
            Rhand_trigger = true;
        }
        if (xx == 2)
        {
            Rhand_trigger = false;
        }
        if (xx == 3)
        {
            Lhand_trigger = true;
        }
        if (xx == 4)
        {
            Lhand_trigger = false;
        }
    }
    IEnumerator wait_hold()
    {
        Rhand.GetComponent<BoxCollider>().enabled = false;
        yield return new WaitForSeconds(3f);
        Rhand.GetComponent<BoxCollider>().enabled = true;
    }
}
